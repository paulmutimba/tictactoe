//Paul Mutimba
//TicTacToe Assignment
#include <iostream>
#include <conio.h>
#include "TicTacToe.h"

using namespace std;

int main()
{
	TicTacToe* pGame = nullptr;

	while (true)
	{
		if (pGame) delete pGame;
		pGame = new TicTacToe;

		// Play the game
		while (!pGame->IsOver())
		{
			pGame->DisplayBoard();

			int position;
			do
			{
				cout << "Player " << pGame->GetPlayerTurn() << ", row by row, select a position (1-9): ";
				cin >> position;
			} while (!pGame->IsValidMove(position));

			pGame->Move(position);
		}

		// Game over
		pGame->DisplayBoard();
		pGame->DisplayResult();


		// Prompt to play again (or quit)
		char input = ' ';
		while (input != 'Y' && input != 'y')
		{
			std::cout << "Would you like to play again? (y/n): ";
			cin >> input;

			if (input == 'N' || input == 'n') return 0; // quit
		}
	}
}

