#include <iostream>
class TicTacToe
{
private:
	char m_playerTurn; //This is used to track if it's X's turn or O's turn.
	char m_winner; //This is used to check to see the winner of the game. A space should be used while the game is being played.
	int m_numTurns; //This will be used to detect when the board is full (all nine spaces are taken).
	char m_board[9] = { ' ',' ',' ',' ',' ',' ',' ',' ',' ' }; //An array used to store the spaces of the game board. Once a player has Moved in one of the positions, the space should change to an X or an O.




public:
	TicTacToe() {

		m_playerTurn = 'x';
		m_winner = ' ';
		m_numTurns = 0;
	};


	void DisplayBoard() { //This method will output the board to the console.
		for (int count = 0; count < 9; count++) {
			if ((count == 2) || (count == 5)) {
				std::cout << m_board[count] << "\n";
				std::cout << "---|----|---\n";
				continue;
			}
			if (count == 8) {
				std::cout << "" << m_board[count] << "\n";
				break;
			}
			std::cout << " " << m_board[count] << " | ";

		}
	}


	char GetPlayerTurn()//returns x or o
	{
		return m_playerTurn;
	}


	void Move(int position)//Places the current players character in the specified position on the board.
	{
		m_board[position - 1] = m_playerTurn; 
		m_numTurns++;
	}

	void DisplayResult() 
	{
		if ((m_numTurns == 9) && (m_winner == ' ')) 
		
		{ 
			std::cout << "Tie!\n";
		}
		else {
			std::cout << m_winner << " won!\n";
		}
	}

	bool IsOver()
	{//Returns true if the game is over (winner or tie), otherwise false if the game is still being played.

		if (m_numTurns != 0)
		{
			if (m_numTurns == 9)
			{
				return true;
			}
			for (int count = 0; count < 8; count = count + 3)
			{
				if ((m_board[count] == m_playerTurn) && (m_board[count + 1] == m_playerTurn) && (m_board[count + 2] == m_playerTurn))
				{
					m_winner = m_playerTurn;
					return true;
				}
			};

			if ((m_board[0] == m_playerTurn) && (m_board[4] == m_playerTurn) && (m_board[8] == m_playerTurn))

			{
				m_winner = m_playerTurn;
				return true;
			}
			for (int count = 0; count < 3; count++) {
				if ((m_board[count] == m_playerTurn) && (m_board[count + 3] == m_playerTurn) && (m_board[count + 6] == m_playerTurn))
				{
					m_winner = m_playerTurn;
					return true;
				}
			};
			if ((m_board[2] == m_playerTurn) && (m_board[4] == m_playerTurn) && (m_board[6] == m_playerTurn))

			{
				m_winner = m_playerTurn;
				return true;
			}


			if (m_playerTurn == 'x')
			{
				m_playerTurn = 'o';
			}
			else {
				m_playerTurn = 'x';
			}

			return false;
		}
		else
		{
			return false;
		}
	}

	bool IsValidMove(int position)
	{
		if (m_board[(position - 1)] == ' ')
		{ // returns true or false
			return true;
		}
		else {
			return false;
		}
	}
};
